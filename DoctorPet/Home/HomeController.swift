//
//  HomeController.swift
//  DoctorPet
//
//  Created by Apple on 22/05/21.
//

import UIKit

class HomeController: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    @IBOutlet weak var viewUpcoming: UIView!
    @IBOutlet weak var viewPending: UIView!

    @IBOutlet weak var tableViewList: UITableView!

    var isUpcomingView = true

    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
    }
    
    func topViewLayout(){
           if !HomeController.hasSafeArea{
               if view_NavConst != nil {
                   view_NavConst.constant = 77
               }
           }
        
        viewUpcoming.isHidden = false
        viewPending.isHidden = true

       }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
          return .lightContent
      }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    
    @IBAction func Upcoming(_ sender: Any) {
        viewUpcoming.isHidden = false
        viewPending.isHidden = true
        isUpcomingView = true
        tableViewList.reloadData()

    }
    
    @IBAction func Pending(_ sender: Any) {
        viewUpcoming.isHidden = true
        viewPending.isHidden = false
        isUpcomingView = false
        tableViewList.reloadData()

    }
    
    
    @IBAction fileprivate func Menu_Action(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SideMenuUpdate"), object: nil)
        sideMenuController?.showLeftView(animated: true, completion: nil)
    }
    
    @IBAction func MySchedule(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CreateScheduleController") as! CreateScheduleController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func MyEarnings(_ sender: Any) {
        let storyboardEarnings = UIStoryboard(name: "Earnings", bundle: nil)
        let vc = storyboardEarnings.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

//MARK: - TableViewDataSource Delegate
extension HomeController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if !isUpcomingView {

            return 1

        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        tableView.separatorColor = .clear
        
        if !isUpcomingView {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PendingCell", for: indexPath) as? PendingCell else {
                    return PendingCell()
                 }
             
             cell.selectionStyle = .none
            
            cell.btnAccept.tag = indexPath.row
            cell.btnAccept.addTarget(self, action: #selector(buttonSelected_Accept), for: .touchUpInside)

             return cell
        }
        else {
            if indexPath.section == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "OnlineCallingTableViewCell", for: indexPath) as? OnlineCallingTableViewCell else {
                        return OnlineCallingTableViewCell()
                     }
                 
                cell.selectionStyle = .none
                if indexPath.row == 1 {
                    cell.lbeSeparator.isHidden = true
                }
                else{
                    cell.lbeSeparator.isHidden = false
                }

                 return cell
            }
            else if (indexPath.section == 1){
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeVisitCallingTableViewCell", for: indexPath) as? HomeVisitCallingTableViewCell else {
                        return HomeVisitCallingTableViewCell()
                     }
                 
                 cell.selectionStyle = .none

                 return cell
            }
            else{
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "ClinicVisitCallingTableViewCell", for: indexPath) as? ClinicVisitCallingTableViewCell else {
                        return ClinicVisitCallingTableViewCell()
                     }
                 
                 cell.selectionStyle = .none
                 return cell
            }
        }
       

  
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if !isUpcomingView {
            
            return UIView()
        }

            let headerView = UIView.init(frame: CGRect.init(x: 18, y: 0, width: tableView.frame.width - 18, height: 50))
            
            let label = UILabel()
            label.frame = CGRect.init(x: 18, y: 0, width: headerView.frame.width-18, height: headerView.frame.height)
    
            label.font = .RegularFont(18)
            label.textColor = UIColor.black
            label.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).lightGrayPlanColor(Alpha: 1)

            if section == 0 {
                label.text = " Online"
            }
            else if (section == 1){
                label.text = " Home Visit"
            }
            else{
                label.text = " Clinic Visit"
            }
        
            headerView.addSubview(label)
            return headerView
        }
    
    
    @objc func buttonSelected_Accept(sender: UIButton){
        print(sender.tag)
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "ConfirmAppointmentController") as! ConfirmAppointmentController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
//MARK: - TableViewDelegate Delegate
extension HomeController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return 150
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
               
        tableView.deselectRow(at: indexPath, animated: false)
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if !isUpcomingView {

            return 0.1

        }
            return 50
        }

}
