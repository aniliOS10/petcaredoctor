//
//  PaymentViewController.swift
//  DoctorPet
//
//  Created by Apple on 31/05/21.
//

import UIKit

class PaymentViewController: UIViewController {
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        topViewLayout()
    }
    
    func topViewLayout(){
           if !ConfirmAppointmentController.hasSafeArea{
               if view_NavConst != nil {
                   view_NavConst.constant = 77
               }
           }
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    @IBAction func Back(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
    

}
