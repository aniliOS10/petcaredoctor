//
//  SettingController.swift
//  DoctorPet
//
//  Created by Apple on 09/06/21.
//

import UIKit

class SettingController: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
    }
    
    func topViewLayout(){
           if !SettingController.hasSafeArea{
               if view_NavConst != nil {
                   view_NavConst.constant = 77
            }
        }
    }
    @IBAction func Back(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {

    }
}
