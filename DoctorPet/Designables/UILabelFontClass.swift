//
//  UILabelFontClass.swift
//  DoctorsTelemed
//
//  Created by dr.mac on 27/11/19.
//  Copyright © 2019 dr.mac. All rights reserved.
//


import UIKit

class UILabelFontClass: UILabel {
    
    @IBInspectable var DynamicFontSize: CGFloat = 0 {
        didSet {
            overrideFontSize(FontSize: DynamicFontSize)
        }
    }
    
    func overrideFontSize(FontSize: CGFloat){
        let fontName = self.font.fontName
        let screenWidth = UIScreen.main.bounds.size.width
        let calculatedFontSize = screenWidth / 375 * FontSize
        self.font = UIFont(name: fontName, size: calculatedFontSize)
    }
}
