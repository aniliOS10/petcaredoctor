//
//  MainController.swift
//  LiveCareDoctor
//
//  Created by Anil Kumar on 17/08/20.
//  Copyright © 2020 Absove. All rights reserved.
//
import UIKit

public let LCLLanguageChangeNotification = "LCLLanguageChangeNotification"


class MainController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(LanguageSet), name: NSNotification.Name( LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.LanguageSet()
    }
    
    @objc func LanguageSet(){
        
        
    }
    
    func languageKey(key: String)->String
    {
       // return LocalizationSystem.sharedInstance.localizedStringForKey(key: key, comment: key)
        
        return key
    }
    
}
extension UIViewController {
    
//    func save(object : LoginObject){
//
//    do {
//        let encodedData = try NSKeyedArchiver.archivedData(withRootObject: object, requiringSecureCoding: false)
//
//        UserDefaults.standard.set(encodedData, forKey: "Login")
//        UserDefaults.standard.synchronize()
//
//
//       } catch {
//           fatalError("Can't encode data: \(error)")
//       }
//
//
//    }
    
    func SessionExpire(){


        let FCSToken = UserDefaults.standard.value(forKey: "DToken")

        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()

        UserDefaults.standard.setValue(FCSToken, forKey: "DToken")
        UserDefaults.standard.synchronize()

        UserDefaults.standard.set(false, forKey: "Login")
        UserDefaults.standard.synchronize()

        RootControllerManager().SetRootViewController()

    }
    
    func SessionExpireAlter()
    {
        let alert = UIAlertController(title: "", message: "Your session has been expired as you are logged in from another device", preferredStyle: .alert)
        
        
        let No = UIAlertAction(title: "OK", style: .default, handler: { action in
            
          //  self.SessionExpire()
            
        })
        alert.addAction(No)
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
   
    
    func popMessageAlert(title:String,message:String,RootView:Bool)
    {
        let alert = UIAlertController(title: title, message:  message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
            
            if RootView == true
            {
                self.navigationController?.popToRootViewController(animated: true)
            }
            else
            {
                self.navigationController?.popViewController(animated: true)
            }
            
            
            
        }))
        self.present(alert, animated: true, completion: {
            
        })
        
    }
    
    
    
}
