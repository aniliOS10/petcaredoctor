//
//  AppDelegate.swift
//  DoctorPet
//
//  Created by Apple on 19/05/21.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import LGSideMenuController

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = UIColor.white

        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
    
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarTintColor = UIColor.black
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        
        RootControllerManager().SetRootViewController()
        
        return true
    }
    
  
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "DoctorPet")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

class RootControllerManager: NSObject {


//MARK: -  @Set RootView Controller
func SetRootViewController(){

    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    if UserDefaults.standard.bool(forKey: "Login")
    {
        SideRootScreen()
    }
    else
    {
        PushToRootScreen(ClassName: storyboard.instantiateViewController(withIdentifier: "SignInController") as! SignInController)

    }
}

//MARK: -  @Push To RootController
func PushToRootScreen(ClassName:UIViewController){
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    let window = appDelegate?.window
    let nav = UINavigationController(rootViewController: ClassName)
    nav.isNavigationBarHidden = true
    window?.rootViewController = nav
    window?.makeKeyAndVisible()
}


func SideRootScreen(){
    let storyboard = UIStoryboard(name: "Home", bundle: nil)
    let leftController = storyboard.instantiateViewController(withIdentifier: "SideMenuController")
                   
    let homecontroller = storyboard.instantiateViewController(withIdentifier: "HomeController")
    let navigationController = UINavigationController(rootViewController: homecontroller)
        navigationController.isNavigationBarHidden = true

    let sideMenu = LGSideMenuController(rootViewController: navigationController,
                                                         leftViewController: leftController,
                                                         rightViewController: nil)
        sideMenu.leftViewWidth = UIScreen.main.bounds.size.width - 110
           
        sideMenu.navigationController?.isNavigationBarHidden = true
        sideMenu.rootViewStatusBarStyle = .lightContent
        sideMenu.leftViewStatusBarStyle = .lightContent
        sideMenu.isLeftViewStatusBarHidden = false
        sideMenu.isLeftViewStatusBarBackgroundVisible = false

        let del = UIApplication.shared.delegate as! AppDelegate
            del.window?.rootViewController = sideMenu
}
}
