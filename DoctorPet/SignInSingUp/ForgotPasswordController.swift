//
//  ForgotPasswordController.swift
//  DoctorPet
//
//  Created by Apple on 22/05/21.
//

import UIKit
import CocoaTextField

class ForgotPasswordController: UIViewController {

    
    @IBOutlet weak var txt_Phone : CocoaTextField!
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        applyStyle(to: txt_Phone)
        txt_Phone.placeholder = "Email or phone"
        txt_Phone.keyboardType = .emailAddress
        txt_Phone.autocapitalizationType = .none
        
        topViewLayout()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
          return .lightContent
      }
    
    func topViewLayout(){
           if !ForgotPasswordController.hasSafeArea{
               if view_NavConst != nil {
                   view_NavConst.constant = 77
               }
           }
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    
    @IBAction func Back(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
    
    //Hide KeyBoard When touche on View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view .endEditing(true)
    }
    
    private func applyStyle(to v: CocoaTextField) {
    
        v.tintColor = UIColor(red: 66/255, green: 132/255, blue: 255/255, alpha: 1)
        v.textColor = UIColor.black
        v.inactiveHintColor = UIColor(red: 93/255, green: 113/255, blue: 137/255, alpha: 1)
        v.activeHintColor = UIColor(red: 93/255, green: 113/255, blue: 137/255, alpha: 1)
        v.focusedBackgroundColor = UIColor.white
        v.defaultBackgroundColor = UIColor.white
        v.borderColor = UIColor.clear
        v.errorColor = UIColor(red: 231/255, green: 76/255, blue: 60/255, alpha: 0.7)
    }
    
   
}
