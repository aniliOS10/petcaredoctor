//
//  SignUpAddressController.swift
//  DoctorPet
//
//  Created by Apple on 01/06/21.
//

import UIKit
import CocoaTextField

class SignUpAddressController: UIViewController {

    @IBOutlet weak var btnGender: UIButton!
    @IBOutlet weak var txt_Gender : CocoaTextField!
    @IBOutlet weak var txt_BirthDate : CocoaTextField!
    @IBOutlet weak var txt_Phone : CocoaTextField!
    @IBOutlet weak var txt_RegNo : CocoaTextField!
    
    @IBOutlet weak var btnStates: UIButton!
    @IBOutlet weak var txt_States: CocoaTextField!
    
    @IBOutlet weak var btnNationalities: UIButton!
    @IBOutlet weak var txt_Nationalities: CocoaTextField!
    
    @IBOutlet weak var btnCountries: UIButton!
    @IBOutlet weak var txt_Countries: CocoaTextField!
    
    @IBOutlet weak var txt_AddressOne: CocoaTextField!
    @IBOutlet weak var txt_AddressTwo: CocoaTextField!
    @IBOutlet weak var txt_City: CocoaTextField!
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    
    var dateCurrent = Date()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        applyStyle(to: txt_BirthDate)
        txt_BirthDate.placeholder = "DOB"
        
        applyStyle(to: txt_Gender)
        txt_Gender.placeholder = "Gender"
        
        applyStyle(to: txt_Phone)
        txt_Phone.placeholder = "Email"
        
        applyStyle(to: txt_AddressOne)
        txt_AddressOne.placeholder = "Address Line 1"
        
        applyStyle(to: txt_AddressTwo)
        txt_AddressTwo.placeholder = "Address Line 2"
        
        applyStyle(to: txt_Countries)
        txt_Countries.placeholder = "Country"
        
        applyStyle(to: txt_States)
        txt_States.placeholder = "State"
        
        applyStyle(to: txt_City)
        txt_City.placeholder = "City"
        
        applyStyle(to: txt_Nationalities)
        txt_Nationalities.placeholder = "Nationality"
        
        applyStyle(to: txt_RegNo)
        txt_RegNo.placeholder = "Registration Number"
        
        
        topViewLayout()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
          return .lightContent
      }
    
    func topViewLayout(){
           if !SignUpAddressController.hasSafeArea{
               if view_NavConst != nil {
                   view_NavConst.constant = 77
               }
           }
    }
    
    //Hide KeyBoard When touche on View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view .endEditing(true)
    }
    
    private func applyStyle(to v: CocoaTextField) {

        v.tintColor = UIColor(red: 66/255, green: 132/255, blue: 255/255, alpha: 1)
        v.textColor = UIColor.black
        v.inactiveHintColor = UIColor(red: 93/255, green: 113/255, blue: 137/255, alpha: 1)
        v.activeHintColor = UIColor(red: 93/255, green: 113/255, blue: 137/255, alpha: 1)
        v.focusedBackgroundColor = UIColor.white
        v.defaultBackgroundColor = UIColor.white
        v.borderColor = UIColor.clear
        v.errorColor = UIColor(red: 231/255, green: 76/255, blue: 60/255, alpha: 0.7)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    @IBAction func Back(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SignUp(_ sender: Any) {
        self.view .endEditing(true)
        
    }
    
    @IBAction func DateOfBrith(_ sender: Any) {

        addPicker()
        
    }
    
    func addPicker(){
       
        let alert = UIAlertController(style: .alert, title: "Select Date of Birth")
        
        if #available(iOS 13.4, *) {
            alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = UIColor.white
            
            alert.setTitle(font:.MediumFont(17), color: UIColor.black)

           alert.view.tintColor = UIColor.black
            
        }
        else{
            alert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).TriadicColor(Alpha: 1)
            
            alert.setTitle(font: .MediumFont(17), color: UIColor.white)
            
            alert.view.tintColor = UIColor.white

        }
        
        let date = Calendar.current.date(byAdding: .year, value: -17, to: Date())
        dateCurrent = date ?? Date()
        
        if self.txt_BirthDate.text == ""
        {
            dateCurrent = date ?? Date()
        }
        else
        {
            let isoDate = txt_BirthDate.text
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            dateCurrent = dateFormatter.date(from:isoDate!)!
        }
        
        alert.addDatePicker(mode: .date, date: dateCurrent, minimumDate: nil, maximumDate: date) { date in
            self.dateCurrent = date
            }
        alert.addAction( title: "OK", style: .default, isEnabled: true) { (action) in
            
            self.txt_BirthDate.text = convertToDDMMyyyy(date:self.dateCurrent)
            
            }
        alert.addAction(title: "Cancel", style: .cancel){ (action) in
            
        }
        
        alert.show()
        
    }
}
