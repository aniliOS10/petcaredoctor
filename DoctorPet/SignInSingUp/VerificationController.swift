//
//  VerificationController.swift
//  DoctorPet
//
//  Created by Apple on 08/06/21.
//

import UIKit
import SVPinView
import Reachability
import IQKeyboardManagerSwift
class VerificationController: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    
    @IBOutlet weak var pinVw: SVPinView!
    @IBOutlet weak var lbeEmail: UILabel!
    
    var userEmailString = ""
    var userPasswordString = ""
    var OTPString = ""
    var OTPValue = 0
    var OTPValueCheck = ""
    var accessToken = ""
    
    //MARK:- Class Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        topViewLayout()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.Verification_Data), name: NSNotification.Name(rawValue: "Verification_Data"), object: nil)
        
        configurePinView()
        
        let doctorReg = UserDefaults.standard.value(forKey: "Verification")
                      print(doctorReg ?? "")
        
        if doctorReg != nil {
            var dict: Dictionary<String, Any> = [:]
                 dict = doctorReg as! Dictionary<String, Any>
             if dict.keys.count > 0 {
                    userEmailString = dict["email"] as! String
                    OTPString = dict["otp"] as! String
                    accessToken = dict["accessToken"] as! String
                    OTPValue = Int(OTPString) ?? 0
                    
                    if userEmailString == ""{
                        userEmailString = "doctorapp@gmail.com"
                    }
                    
                    lbeEmail.text = userEmailString
                    
                    if OTPValue != 0{
                        self.MessageAlert(title: "OTP", message: String(OTPValue))
                    }
                 
//                 let prefs = UserDefaults.standard
//                     prefs.removeObject(forKey:"Verification")
             }
        }
    }
    
    
    func topViewLayout(){
           if !VerificationController.hasSafeArea{
               if view_NavConst != nil {
                   view_NavConst.constant = 77
               }
           }
    }


       @objc func Verification_Data(_ notification: NSNotification) {

           let doctorReg = UserDefaults.standard.value(forKey: "Verification")
                         print(doctorReg ?? "")
           
           if doctorReg != nil {
               var dict: Dictionary<String, Any> = [:]
                    dict = doctorReg as! Dictionary<String, Any>
                if dict.keys.count > 0 {
                       userEmailString = dict["email"] as! String
                       OTPString = dict["otp"] as! String
                       accessToken = dict["accessToken"] as! String
                       OTPValue = Int(OTPString) ?? 0
                       
                       if userEmailString == ""{
                           userEmailString = "doctorapp@gmail.com"
                       }
                       
                       lbeEmail.text = userEmailString
                       
                       if OTPValue != 0{
                           self.MessageAlert(title: "OTP", message: String(OTPValue))
                       }
                }
           }
           
       }

       //MARK:-   Configure PinView Function
       func configurePinView() {
           
           pinVw.style = .box
           pinVw.borderLineColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).grayPlanColor(Alpha: 1)
           pinVw.activeBorderLineColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).grayPlanColor(Alpha: 1)
           pinVw.keyboardType = .numberPad
           pinVw.shouldSecureText = false
           pinVw.pinInputAccessoryView = { () -> UIView in
               let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
               doneToolbar.barStyle = UIBarStyle.default
               let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
               let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(dismissKeyboard))
               
               var items = [UIBarButtonItem]()
               items.append(flexSpace)
               items.append(done)
               
               doneToolbar.items = items
               doneToolbar.sizeToFit()
               return doneToolbar
           }()
           
           pinVw.didChangeCallback = { pin in
               print("The entered pin is \(pin)")
               
               if pin == self.OTPString{
                   
                   self.OTPValueCheck = String(self.OTPString)
                   
               }
               else{
                   self.OTPValueCheck = String(pin)
               }
           }
       }
       
       //MARK:-  Dismiss Keyboard Action
       @objc func dismissKeyboard() {
           self.view.endEditing(false)
       }
       //MARK:-    touches Began KEYBOARD Hide
       override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           
           self.view.endEditing(true)
           
       }
       
       //MARK:-  Resend Button  Action
       @IBAction func Resend(_ sender: Any) {
               self.configurePinView()
               let params = ["email":self.userEmailString] as [String : Any]
               self.callResendEmailApi(param: params)
          
       }
       
       
       
     
       
       //MARK:-  Continute Button Action
       @IBAction func Done(_ sender: Any) {
           
           if String(self.OTPValueCheck).count < 4{
               
               self.MessageAlert(title: "", message:"Please Verify your OTP")
               return
           }
                   let params = ["email":self.userEmailString,"otp": String(self.OTPValueCheck)] as [String : Any]
           
        
           self.callConfirmEmailApi(param: params)
       }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
          return .lightContent
      }
       
       //MARK:-  Cancel Button Acxtion To PopToView
       @IBAction func Cancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
       }
       
       //MARK:-  Resend OTP on Email
       func callResendEmailApi(param : [String : Any]){
//
//           ResendEmailAPIRequest.shared.ResendEmail(requestParams: param, accessToken: accessToken) { (message, status,otp) in
//
//               if status == true{
//                   self.MessageAlert(title:message!, message: String(otp))
//               }
//               else
//               {
//                   self.MessageAlert(title: "", message: message!)
//               }
//           }
           
       }
       
       //MARK:-  Verification OTP
       func callConfirmEmailApi(param : [String : Any]){
           
//           ConfirmEmailAPIRequest.shared.ConfirmEmail(requestParams: param, accessToken: accessToken) { (message, status) in
//               if status == true{
//
//                    UserDefaults.standard.set(self.accessToken, forKey: "accessToken_SignUp")
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue:"Verification_Continue_Action"), object: nil, userInfo: nil)
//
//                    let prefs = UserDefaults.standard
//                        prefs.removeObject(forKey:"Verification")
//
//               }
//               else
//               {
//                  self.MessageAlert(title: "", message: message!)
//               }
//           }
//
//       }
       
   }

}
