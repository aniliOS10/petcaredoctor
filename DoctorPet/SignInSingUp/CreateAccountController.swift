//
//  CreateAccountController.swift
//  DoctorPet
//
//  Created by Apple on 21/05/21.
//

import UIKit
import CocoaTextField

class CreateAccountController: UIViewController {

    @IBOutlet weak var txt_First : CocoaTextField!
    @IBOutlet weak var txt_Last : CocoaTextField!
    @IBOutlet weak var txt_Phone : CocoaTextField!
    @IBOutlet weak var txt_Password : CocoaTextField!
    @IBOutlet weak var txt_ConfirmPass : CocoaTextField!

    
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyStyle(to: txt_First)
        txt_First.placeholder = "First Name"
        txt_First.keyboardType = .emailAddress
        
        applyStyle(to: txt_Last)
        txt_Last.placeholder = "Last Name"
        
        applyStyle(to: txt_Phone)
        txt_Phone.placeholder = "Phone No"
        
        applyStyle(to: txt_Password)
        txt_Password.placeholder = "Password"
        
        applyStyle(to: txt_ConfirmPass)
        txt_ConfirmPass.placeholder = "Confirm Password"
        
        topViewLayout()
    }
    
    func topViewLayout(){
           if !CreateAccountController.hasSafeArea{
               if view_NavConst != nil {
                   view_NavConst.constant = 77
               }
           }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
          return .lightContent
      }
    
    //Hide KeyBoard When touche on View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view .endEditing(true)
    }
    
    private func applyStyle(to v: CocoaTextField) {

        v.tintColor = UIColor(red: 66/255, green: 132/255, blue: 255/255, alpha: 1)
        v.textColor = UIColor.black
        v.inactiveHintColor = UIColor(red: 93/255, green: 113/255, blue: 137/255, alpha: 1)
        v.activeHintColor = UIColor(red: 93/255, green: 113/255, blue: 137/255, alpha: 1)
        v.focusedBackgroundColor = UIColor.white
        v.defaultBackgroundColor = UIColor.white
        v.borderColor = UIColor.clear
        v.errorColor = UIColor(red: 231/255, green: 76/255, blue: 60/255, alpha: 0.7)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    @IBAction func Back(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func SignUp(_ sender: Any) {
        self.view .endEditing(true)
        
        let nameText = txt_First.text ?? ""
        let surNameText = txt_Last.text ?? ""
        let password = txt_Password.text ?? ""
        let phone = txt_Phone.text ?? ""
    
       if nameText.isEmpty
        {
            MessageAlert(title:"",message: "Please enter in First Name")
            return
        }
        else if surNameText.isEmpty
        {
            MessageAlert(title:"",message: "Please enter in Last Name")
            return
        }
        else if phone.isEmpty
        {
            MessageAlert(title:"",message: "Please enter in Phone No")
            return
        }
        else if password.isEmpty
        {
            MessageAlert(title:"",message: "Please enter in Password")
            return
        }
        
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "SignUpAddressController") as! SignUpAddressController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}
