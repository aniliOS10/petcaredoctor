//
//  SignInController.swift
//  DoctorPet
//
//  Created by Apple on 21/05/21.
//

import UIKit
import CocoaTextField

class SignInController: UIViewController {

    
    @IBOutlet weak var txt_Phone : CocoaTextField!
    @IBOutlet weak var txt_Password : CocoaTextField!

    @IBOutlet weak var btn_Signin : UIButton!
    @IBOutlet weak var btn_Forgot : UIButton!
    @IBOutlet weak var btn_Signup : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyStyle(to: txt_Phone)
        txt_Phone.placeholder = "Email or phone"
        txt_Phone.keyboardType = .emailAddress
        txt_Phone.autocapitalizationType = .none
        
        applyStyle(to: txt_Password)
        txt_Password.placeholder = "Password"
        txt_Password.isSecureTextEntry = true

    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
          return .lightContent
      }
    
    //Hide KeyBoard When touche on View
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view .endEditing(true)
    }
    
    private func applyStyle(to v: CocoaTextField) {
    
        v.tintColor = UIColor(red: 66/255, green: 132/255, blue: 255/255, alpha: 1)
        v.textColor = UIColor.black
        v.inactiveHintColor = UIColor(red: 93/255, green: 113/255, blue: 137/255, alpha: 1)
        v.activeHintColor = UIColor(red: 93/255, green: 113/255, blue: 137/255, alpha: 1)
        v.focusedBackgroundColor = UIColor.white
        v.defaultBackgroundColor = UIColor.white
        v.borderColor = UIColor.clear
        v.errorColor = UIColor(red: 231/255, green: 76/255, blue: 60/255, alpha: 0.7)
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    @IBAction fileprivate func btnSignUpAction(_ sender: Any) {
        self.view .endEditing(true)
        let vc = storyboard?.instantiateViewController(withIdentifier: "CreateAccountController") as! CreateAccountController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction fileprivate func btnForgotAction(_ sender: Any) {
        self.view .endEditing(true)
        let vc = storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordController") as! ForgotPasswordController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction fileprivate func btnSignInAction(_ sender: Any) {
        self.view .endEditing(true)

        let trimmedEmailName = txt_Phone.text?.trimmingCharacters(in: .whitespacesAndNewlines)

        let trimmedPassword = txt_Password.text?.trimmingCharacters(in: .whitespacesAndNewlines)

        if (trimmedEmailName?.isEmpty)!{
            MessageAlert(title:"",message: "Please enter your phone no without country code.")
            return
        }
        if let validphone = trimmedEmailName, validphone.isValidPhone() || validphone.EmailValidation() {
                    print("Success")
              }else{
                    print("Enter either valid phone or email 2")
                    MessageAlert(title:"",message: "Please enter your phone no without country code.")
                    return
        }
        if (trimmedPassword?.isEmpty)!{
                    MessageAlert(title:"",message: "Please enter your password.")
                    return
        }
        
        
        loginUserAPI()
        
    }
    
    
    func loginUserAPI(){
        RootControllerManager().SideRootScreen()
        txt_Phone.text = ""
        txt_Password.text = ""
        UserDefaults.standard.set(true, forKey: "Login")
        UserDefaults.standard.synchronize()
    }

    
}
extension SignInController : UITextFieldDelegate
{
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (string == " ") {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
}
extension UIViewController
{
    static var hasSafeArea: Bool {
        guard #available(iOS 13.0, *), let topPadding = UIApplication.shared.keyWindow?.safeAreaInsets.top, topPadding > 24 else {
            return false
        }
        return true
    }

    
    func MessageAlert(title:String,message:String)
    {
        let alert = UIAlertController(title:"Oops!", message:  message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        self.present(alert, animated: true, completion: {
            
        })
        
    }
    
    
    func popMessageAlert(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message:  message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
            
            self.navigationController?.popViewController(animated: true)
            
        }))
        self.present(alert, animated: true, completion: {
            
        })
        
    }
    
    func SessionMessageAlert(title:String,message:String)
    {
        let alert = UIAlertController(title: title, message:  message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title:"OK" , style: .cancel, handler:{ (UIAlertAction)in
        
            NotificationCenter.default.post(name: Notification.Name("SessionExpire"), object: nil)
            
        }))
        self.present(alert, animated: true, completion: {
            
        })
        
    }
}
