//
//  OnlineShiftController.swift
//  DoctorPet
//
//  Created by Apple on 07/06/21.
//

import UIKit

class OnlineShiftController: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    @IBOutlet weak var lbeDate: UILabel!

    var selectDate_Title = ""
    var currentDate = ""
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lbeDate.text = selectDate_Title
        topViewLayout()
    }
    
    func topViewLayout(){
           if !OnlineScheduleController.hasSafeArea{
               if view_NavConst != nil {
                   view_NavConst.constant = 77
               }
           }
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    @IBAction func Back(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK: - TableViewDataSource Delegate
extension OnlineShiftController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "OnlineShiftCell", for: indexPath) as? OnlineShiftCell else {
                return OnlineShiftCell()
            }
         
         cell.selectionStyle = .none
         cell.lbeShift.text = String(format: "Shift %d", indexPath.row + 1)

         return cell
    }
}
//MARK: - TableViewDelegate Delegate
extension OnlineShiftController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return 80
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
               
        tableView.deselectRow(at: indexPath, animated: false)
        
    }

}
