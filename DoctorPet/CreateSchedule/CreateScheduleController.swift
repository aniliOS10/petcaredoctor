//
//  CreateScheduleController.swift
//  DoctorPet
//
//  Created by Apple on 27/05/21.
//

import UIKit

class CreateScheduleController: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        topViewLayout()
    }
    
    func topViewLayout(){
           if !CreateScheduleController.hasSafeArea{
               if view_NavConst != nil {
                   view_NavConst.constant = 77
               }
           }
    }
    override func viewWillAppear(_ animated: Bool) {

    }
    
    @IBAction func Online_Schedule(_ sender: Any) {

        let vc = storyboard?.instantiateViewController(withIdentifier: "OnlineScheduleController") as! OnlineScheduleController
        self.navigationController?.pushViewController(vc, animated: true)
    
    }
    
    @IBAction func Home_Schedule(_ sender: Any) {

        
    }
    
    @IBAction func Clinic_Schedule(_ sender: Any) {

        let vc = storyboard?.instantiateViewController(withIdentifier: "ClinicVisitScheduleController") as! ClinicVisitScheduleController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func Back(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
}
