//
//  OnlineScheduleController.swift
//  DoctorPet
//
//  Created by Apple on 27/05/21.
//

import UIKit
import EventKit

class OnlineScheduleController: UIViewController,CalendarViewDataSource,CalendarViewDelegate {
    
    @IBOutlet weak var calenderVw: CalendarView!
    @IBOutlet weak var btnNext_M: UIButton!
    @IBOutlet weak var btnLast_M: UIButton!
    @IBOutlet weak var imgLeftArr: UIImageView!
    @IBOutlet weak var imgRightArr: UIImageView!
    @IBOutlet var scrool: UIScrollView!
    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    var dateSelectArray  = [String] ()

    override func viewDidLoad() {
        super.viewDidLoad()
        addCalender()
        topViewLayout()
        
    }
    
    func topViewLayout(){
           if !OnlineScheduleController.hasSafeArea{
               if view_NavConst != nil {
                   view_NavConst.constant = 77
               }
           }
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    
    
    func addCalender(){
        
        CalendarView.Style.cellShape                = .square
        CalendarView.Style.cellColorDefault         = UIColor.clear
        CalendarView.Style.headerTextColor          = UIColor.black
        CalendarView.Style.cellTextColorDefault     = UIColor.black
        CalendarView.Style.cellTextColorToday       = UIColor.black
        CalendarView.Style.cellTextColorWeekend     = UIColor.black
        CalendarView.Style.cellSelectedColor        = UIColor.black
        CalendarView.Style.cellSelectedBorderColor  = UIColor.black
        CalendarView.Style.cellBorderColor          = UIColor.black
        CalendarView.Style.cellBorderWidth          = 0
        CalendarView.Style.firstWeekday             = .sunday
        CalendarView.Style.locale                   = Locale(identifier: "en_US")
        CalendarView.Style.timeZone = TimeZone.current
        calenderVw.direction                        = .horizontal
        calenderVw.dataSource                       = self
        calenderVw.delegate                         = self
        calenderVw.multipleSelectionEnable          = false
        calenderVw.marksWeekends                    = false
        calenderVw.enableDeslection                 = false
        
        calenderVw.setDisplayDate(Date())

        btnNext_M.addTarget(self, action: #selector(OnlineScheduleController.nextMonth), for: .touchUpInside)
        btnLast_M.addTarget(self, action: #selector(OnlineScheduleController.previousMonth), for: .touchUpInside)
        
        let today = Date()
        self.calenderVw.selectDate(today)
        self.calenderVw.setDisplayDate(today)
        
        self.calenderVw.collectionView.reloadData()
        
        getDataOnline(Date(), true)
    }
    
    
    @objc func nextMonth(sender: UIButton)
    {
        self.calenderVw.goToNextMonth()
    }
    
    @objc func previousMonth(sender: UIButton)
    {
        self.calenderVw.goToPreviousMonth()
    }
    
    
    // MARK : KDCalendarDataSource
    
    func startDate() -> Date {
        
        var dateComponents = DateComponents()
        dateComponents.month = -1
        let today = Date()
        return today
    }
    
    func endDate() -> Date {
        
        var dateComponents = DateComponents()
        dateComponents.month = 1
        let today = Date()
        let twoYearsFromNow = self.calenderVw.calendar.date(byAdding: dateComponents, to: today)!
        
        return twoYearsFromNow
        
    }
    
    @IBAction func Back(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
    
    // MARK : KDCalendarDelegate
    func calendar(_ calendar: CalendarView, didSelectDate date: Date, withEvents events: [CalendarEvent]) {
        
    }
    
    func calendar(_ calendar: CalendarView, didScrollToMonth date : Date) {

    }
    
    
    func calendar(_ calendar : CalendarView, canSelectDate date : Date) -> Bool{
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        formatter.timeZone = TimeZone.current

        let dateStr = formatter.string(from: date)
        
        let CurrentDate = Date()
                  formatter.dateFormat = "dd-MM-yyyy"
                  let todayStr = formatter.string(from: CurrentDate)
        if dateSelectArray.contains(dateStr){
    
            if dateStr == todayStr {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "LLLL"
                dateFormatter.timeZone = TimeZone.current
                let nameOfMonth = dateFormatter.string(from: date)
                
                dateFormatter.dateFormat = "dd"
                let days = dateFormatter.string(from: date)
              //  isLoadDataOnBack = true
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "OnlineShiftController") as! OnlineShiftController
               vc.selectDate_Title = String(format: "%@ %@", days,nameOfMonth)
               vc.currentDate = dateStr
                self.navigationController?.pushViewController(vc, animated: true)
                return true
                
            }
            else if CurrentDate < date {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "LLLL"
                dateFormatter.timeZone = TimeZone.current

                let nameOfMonth = dateFormatter.string(from: date)
                dateFormatter.dateFormat = "dd"
                let days = dateFormatter.string(from: date)
               // isLoadDataOnBack = true
                let storyboard = UIStoryboard(name: "Home", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "OnlineShiftController") as! OnlineShiftController
                vc.selectDate_Title = String(format: "%@ %@", days,nameOfMonth)
                vc.currentDate = dateStr
                self.navigationController?.pushViewController(vc, animated: true)
                return true
            }
            
          return false
           
        }
        else{
            
            if dateStr == todayStr {
                createNewSlotOnDate(date: date)
                return true

            }
             else if CurrentDate < date {
                createNewSlotOnDate(date: date)
                return true
            }
            return false

        }
    }
    
    
    func createNewSlotOnDate(date : Date){
        
        let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            formatter.timeZone = TimeZone.current
        let dateStr = formatter.string(from: date)
                      formatter.dateFormat = "LLLL"
                      formatter.timeZone = TimeZone.current

        let nameOfMonth = formatter.string(from: date)
                        formatter.dateFormat = "dd"
        let days = formatter.string(from: date)
                   //   isLoadDataOnBack = true
        
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CreateTimeSlotsController") as! CreateTimeSlotsController
            vc.selectDate_Title = String(format: "%@ %@", days,nameOfMonth)
            vc.currentDate = dateStr
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func getDataOnline(_ date:Date, _ isLoader:Bool){

        dateSelectArray = ["10-06-2021","15-06-2021","20-06-2021","25-06-2021","26-06-2021","27-06-2021","28-06-2021","29-06-2021","30-06-2021",]
        
        self.calenderVw.bookedSlotDate =  dateSelectArray
        self.calenderVw.collectionView.reloadData()
        
    }
}
