//
//  ConfirmAppointmentController.swift
//  DoctorPet
//
//  Created by Apple on 09/06/21.
//

import UIKit

class ConfirmAppointmentController: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()

        topViewLayout()
    }
    
    func topViewLayout(){
           if !ConfirmAppointmentController.hasSafeArea{
               if view_NavConst != nil {
                   view_NavConst.constant = 77
               }
           }
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    @IBAction func Back(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
    

}
