//
//  PastAppointmentsController.swift
//  DoctorPet
//
//  Created by Apple on 26/05/21.
//

import UIKit

class PastAppointmentsController: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!
    
    @IBOutlet weak var lbeOnline: UILabel!
    @IBOutlet weak var lbeHome: UILabel!
    @IBOutlet weak var lbeClinic: UILabel!
    
    @IBOutlet weak var viewContainer: UIView!

    var vcOnline: OnlinePastAppointmentsController?
    var vcHome: HomePastAppointmentsController?
    var vcClinic: ClinicPastAppointmentsController?
    
    var nav_Online: UINavigationController?
    var nav_Home: UINavigationController?
    var nav_Clinic: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        topViewLayout()
        load_Call()
    }
    
    func topViewLayout(){
           if !PastAppointmentsController.hasSafeArea{
               if view_NavConst != nil {
                    view_NavConst.constant = 77
               }
           }
       }
    
    
    // MARK: - load View
    func load_Call(){
                
        vcOnline = (self.storyboard!.instantiateViewController(withIdentifier: "OnlinePastAppointmentsController") as! OnlinePastAppointmentsController)
        
        vcHome = (self.storyboard!.instantiateViewController(withIdentifier: "HomePastAppointmentsController") as! HomePastAppointmentsController)
            
        vcClinic = (self.storyboard!.instantiateViewController(withIdentifier: "ClinicPastAppointmentsController") as! ClinicPastAppointmentsController)
        
        nav_Online = UINavigationController(rootViewController: vcOnline!)
        nav_Home = UINavigationController(rootViewController: vcHome!)
        nav_Clinic = UINavigationController(rootViewController: vcClinic!)
        
        addChild(nav_Online!)
        addChild(nav_Home!)
        addChild(nav_Clinic!)
        
        viewContainer .addSubview((nav_Online?.view)!)
        viewContainer .addSubview((nav_Home?.view)!)
        viewContainer .addSubview((nav_Clinic?.view)!)
        
        nav_Online!.didMove(toParent: self)
        nav_Home!.didMove(toParent: self)
        nav_Clinic!.didMove(toParent: self)

        viewContainer.bringSubviewToFront(nav_Online!.view)

        
    }

    // MARK: - viewDidLayoutSubviews
      override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        nav_Online!.view.frame = viewContainer.bounds
        nav_Home!.view.frame = viewContainer.bounds
        nav_Clinic!.view.frame = viewContainer.bounds
      }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    @IBAction func Back(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completion: nil)
    }
    
    @IBAction func Online_Action(_ sender: Any) {

        lbeOnline.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).TriadicColor(Alpha: 1)
        lbeHome.backgroundColor = UIColor.clear
        lbeClinic.backgroundColor = UIColor.clear
        
        lbeOnline.textColor = UIColor.white
        lbeHome.textColor = UIColor.black
        lbeClinic.textColor = UIColor.black
        
        viewContainer.bringSubviewToFront(nav_Online!.view)


    }
    
    @IBAction func Home_Action(_ sender: Any) {
        lbeOnline.backgroundColor = UIColor.clear
        lbeHome.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).TriadicColor(Alpha: 1)
        lbeClinic.backgroundColor = UIColor.clear
        
        lbeOnline.textColor = UIColor.black
        lbeHome.textColor = UIColor.white
        lbeClinic.textColor = UIColor.black
        
        viewContainer.bringSubviewToFront(nav_Home!.view)

    }
    
    @IBAction func Clinic_Action(_ sender: Any) {
        lbeOnline.backgroundColor = UIColor.clear
        lbeHome.backgroundColor = UIColor.clear
        lbeClinic.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).TriadicColor(Alpha: 1)
        
        lbeOnline.textColor = UIColor.black
        lbeHome.textColor = UIColor.black
        lbeClinic.textColor = UIColor.white
        
        viewContainer.bringSubviewToFront(nav_Clinic!.view)

    }
}
