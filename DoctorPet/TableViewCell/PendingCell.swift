//
//  PendingCell.swift
//  DoctorPet
//
//  Created by Apple on 07/06/21.
//

import UIKit

class PendingCell: UITableViewCell {

    
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnViewDetail: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
