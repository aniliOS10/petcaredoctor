//
//  OnlineShiftCell.swift
//  DoctorPet
//
//  Created by Apple on 07/06/21.
//

import UIKit

class OnlineShiftCell: UITableViewCell {

    
    @IBOutlet weak var lbeShift: UILabel!
    @IBOutlet weak var lbeTime: UILabel!
    
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnDeleted: UIButton!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
