//
//  OnlineCallingTableViewCell.swift
//  DoctorPet
//
//  Created by Apple on 04/06/21.
//

import UIKit

class OnlineCallingTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lbeSeparator: UILabel!
    @IBOutlet weak var lbeName: UILabel!
    @IBOutlet weak var lbeAddress: UILabel!

    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
