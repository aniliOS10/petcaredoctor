//
//  SideMenuController.swift
//  DoctorPet
//
//  Created by Apple on 27/05/21.
//

import UIKit
import LGSideMenuController

class SideMenuController:UIViewController ,UITableViewDelegate,UITableViewDataSource{
   
        @IBOutlet weak var tableview: UITableView!
        @IBOutlet weak var lbeName: UILabel!
        @IBOutlet weak var lbeSp: UILabel!
        @IBOutlet weak var lbe_AppBuild: UILabel!
        @IBOutlet weak var imgProfile: UIImageView!

        var titleArray = [String]()
        var imagesArray = [String]()
        
    override func viewDidLoad() {
        super.viewDidLoad()
            
            titleArray = ["Home","Past Appointments","My Profile","Settings","Sign Out"]
            imagesArray = ["home_ic","past_ic","profile_ic","setting_ic","logout_ic"]
            
            if #available(iOS 11.0, *) {
                tableview.contentInsetAdjustmentBehavior = .never
            }

            NotificationCenter.default.addObserver(self, selector: #selector(self.SideMenuUpdate), name: NSNotification.Name(rawValue: "SideMenuUpdate"), object: nil)
       }
    
    @objc func SideMenuUpdate(_ notification: NSNotification) {
            self.tableview.reloadData()
        }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titleArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell") as! MenuTableViewCell
        
        cell.lbeName.text = titleArray[indexPath.row]
        cell.imgVw.image = UIImage (named: imagesArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         
      tableview.deselectRow(at: indexPath, animated: false)

      if indexPath.row == 0 {
        
        let storyboardProfile = UIStoryboard(name: "Home", bundle: nil)
        let controller = storyboardProfile.instantiateViewController(withIdentifier: "HomeController")
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.isNavigationBarHidden = true
                                  
        sideMenuController?.rootViewController = navigationController
      }
        
       if indexPath.row == 1 {
        let storyboardProfile = UIStoryboard(name: "Home", bundle: nil)
        let controller = storyboardProfile.instantiateViewController(withIdentifier: "PastAppointmentsController")
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.isNavigationBarHidden = true
                                  
        sideMenuController?.rootViewController = navigationController
            
        }
        
        
      if indexPath.row == 2 {
            let storyboardProfile = UIStoryboard(name: "MyProfile", bundle: nil)
            let controller = storyboardProfile.instantiateViewController(withIdentifier: "MyProfileController")
            let navigationController = UINavigationController(rootViewController: controller)
            navigationController.isNavigationBarHidden = true
                                      
            sideMenuController?.rootViewController = navigationController
        }
        
        
        if indexPath.row == 3 {
              let storyboardProfile = UIStoryboard(name: "MyProfile", bundle: nil)
              let controller = storyboardProfile.instantiateViewController(withIdentifier: "SettingController")
              let navigationController = UINavigationController(rootViewController: controller)
              navigationController.isNavigationBarHidden = true
                                        
              sideMenuController?.rootViewController = navigationController
              
          }
        
        sideMenuController?.hideLeftView()

        
        if titleArray.count - 1 == indexPath.row {
            ActionSheet()
        }
        
    }
    
    //MARK: - Logout Message Action Sheet
    func ActionSheet()
    {
        
        let alert = UIAlertController(title: nil, message:"Are you sure you want to sign out?", preferredStyle: .alert)
        
        let No = UIAlertAction(title:"No", style: .default, handler: { action in
        })
            alert.addAction(No)
        
        let Yes = UIAlertAction(title:"Yes", style: .default, handler: { action in
        
            UIViewControllerX().SessionExpire()


        })
        alert.addAction(Yes)
        
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
        
    }

    
}
