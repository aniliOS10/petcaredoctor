//
//  MyProfileController.swift
//  DoctorPet
//
//  Created by Apple on 27/05/21.
//

import UIKit

class MyProfileController: UIViewController {

    @IBOutlet weak var view_NavConst: NSLayoutConstraint!

    @IBOutlet weak var viewPersonal: UIView!
    @IBOutlet weak var viewWorking: UIView!
    
    @IBOutlet weak var viewPersonalData: UIView!
    @IBOutlet weak var viewWorkingData: UIView!


    override func viewDidLoad() {
        super.viewDidLoad()
        viewWorking.isHidden = true
        viewPersonal.isHidden = false
        viewPersonalData.isHidden = false
        viewWorkingData.isHidden = true

        
        topViewLayout()
    }
    
    func topViewLayout(){
           if !MyProfileController.hasSafeArea{
               if view_NavConst != nil {
                   view_NavConst.constant = 77
               }
           }
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    @IBAction func Menu_Action(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completion: nil)

    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
          return .lightContent
      }
    

    @IBAction func Working(_ sender: Any) {
        viewWorking.isHidden = false
        viewPersonal.isHidden = true
        viewPersonalData.isHidden = true
        viewWorkingData.isHidden = false

    }
    
    @IBAction func Personal(_ sender: Any) {
        viewWorking.isHidden = true
        viewPersonal.isHidden = false
        viewPersonalData.isHidden = false
        viewWorkingData.isHidden = true

    }
}
