//
//  PendingAppointmentsController.swift
//  DoctorPet
//
//  Created by Apple on 26/05/21.
//

import UIKit

class PendingAppointmentsController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    @IBAction func Back(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
}

/*
 //MARK: - TableViewDataSource Delegate
 extension PendingAppointmentsController : UITableViewDataSource
 {
     func numberOfSections(in tableView: UITableView) -> Int {
     
         return 1
     }
     
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         

             return self.PendingAppointments.count
       
     }
     

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
             return CellFunction(indexPath:indexPath)
     
     }
     
     func CellFunction(indexPath: IndexPath)-> UITableViewCell
        {
         guard let cellOfUp = tableView_P.dequeueReusableCell(withIdentifier: "PendingAppCell", for: indexPath) as? PendingAppCell else {
                    return PendingAppCell()
                }
         cellOfUp.selectionStyle = .none
         
         
         let data : PendingAppointmentModel = self.PendingAppointments[indexPath.row]

         cellOfUp.lbePatientName.text = data.patientName

         let startDate = data.startTime.UTCToLocal(incomingFormat: "dd-MM-yyyy HH:mm:ss", outGoingFormat: "h:mm a")
              
         let finalDate = data.endTime.UTCToLocal(incomingFormat: "dd-MM-yyyy HH:mm:ss", outGoingFormat: "h:mm a")
              
         cellOfUp.lbeTime.text = String(format: "%@ - %@",startDate,finalDate)
             
         cellOfUp.lbeDate.text = data.startTime.UTCToLocal(incomingFormat: "dd-MM-yyyy HH:mm:ss", outGoingFormat: "dd-MM-yyyy")

         let img = GlobalConstants.ImageBaseURL  + data.patientProfilePic
               let escapedAddress = img.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
               var strURL = String()
               strURL = escapedAddress ?? ""
                           
         cellOfUp.imgView.sd_setImage(with:URL(string: strURL), placeholderImage: UIImage(named: GlobalConstants.MalePlaceHolding))

                return cellOfUp
     }
     
 }
     
 //MARK: - TableViewDelegate Delegate
 extension PendingAppointmentsController : UITableViewDelegate
 {
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
         return 180
        
     }
     
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         tableView.deselectRow(at: indexPath, animated: false)

         let data : PendingAppointmentModel = self.PendingAppointments[indexPath.row]

         let storyboard = UIStoryboard(name: "Appointment", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "AcceptAppController") as! AcceptAppController
         vc.appointmentId = String(data.appointmentId)
         vc.patientId = data.patientId
         vc.appointmentStats = data.appointmentStatus
         let startDate = data.startTime.UTCToLocal(incomingFormat: "dd-MM-yyyy HH:mm:ss", outGoingFormat: "dd-MM-yyyy HH:mm:ss")
         vc.appointmentTime = startDate
         self.navigationController?.pushViewController(vc, animated: true)
         
     }

 }

 */
