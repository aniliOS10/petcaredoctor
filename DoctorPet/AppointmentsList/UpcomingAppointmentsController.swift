//
//  UpcomingAppointmentsController.swift
//  DoctorPet
//
//  Created by Apple on 26/05/21.
//

import UIKit

class UpcomingAppointmentsController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {

    }
    @IBAction func Back(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
}
/*
//MARK: - TableViewDataSource Delegate
extension UpcomingAppointmentsController : UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
    
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        

        if UpcomingAppointments.count == 0 {
            btnSeeMore.isHidden = true
        }
        else{
            btnSeeMore.isHidden = false
        }
        
        return self.UpcomingAppointments.count

    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       guard let cellOfUp = tableView_Up.dequeueReusableCell(withIdentifier: "UpComingAppCell", for: indexPath) as? UpComingAppCell else {
                         return UpComingAppCell()
                     }
        
        cellOfUp.selectionStyle = .none
        
        let data : UpcomingAppointmentModel = self.UpcomingAppointments[indexPath.row]

        cellOfUp.lbePatientName.text = data.patientName

        let startDate = data.startTime.UTCToLocal(incomingFormat: "dd-MM-yyyy HH:mm:ss", outGoingFormat: "h:mm a")
             
        let finalDate = data.endTime.UTCToLocal(incomingFormat: "dd-MM-yyyy HH:mm:ss", outGoingFormat: "h:mm a")
             
        cellOfUp.lbeTime.text = String(format: "%@ - %@",startDate,finalDate)
            
        cellOfUp.lbeDate.text = data.startTime.UTCToLocal(incomingFormat: "dd-MM-yyyy HH:mm:ss", outGoingFormat: "dd-MM-yyyy")


        let img = GlobalConstants.ImageBaseURL  + data.patientProfilePic
              let escapedAddress = img.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
              var strURL = String()
              strURL = escapedAddress ?? ""
                          
        cellOfUp.imgView.sd_setImage(with:URL(string: strURL), placeholderImage: UIImage(named: GlobalConstants.MalePlaceHolding))
        
        
        cellOfUp.btnEst.tag = indexPath.row
        cellOfUp.btnEst.addTarget(self, action: #selector(buttonSelected_Est), for: .touchUpInside)
        
        
        cellOfUp.btnMore.tag = indexPath.row
        cellOfUp.btnMore.addTarget(self, action: #selector(buttonSelected_More), for: .touchUpInside)
        
      

        let startEst = data.startTime.UTCToLocal(incomingFormat: "dd-MM-yyyy HH:mm:ss", outGoingFormat: "dd-MM-yyyy HH:mm:ss")
        
        cellOfUp.viewJoinCall.isHidden = false
        cellOfUp.lbeEst.isHidden = false
        cellOfUp.imgViewTimeLogo.isHidden = false
        cellOfUp.btnEst.isHidden = true

        let time =  getTimeSendNewDate(startEst)
        
        
        if time.0 > 0 {
            
            if time.0 == 1 {
                cellOfUp.lbeTimeH.text = String(format: "%02d Day", time.0)
            }
            else{
                cellOfUp.lbeTimeH.text = String(format: "%02d Days", time.0)
            }
            cellOfUp.lbeTimeS.text = ""
            cellOfUp.lbeTimeM.text = ""
            cellOfUp.viewJoinCall.backgroundColor = AppColor.AppGreenishTeal

        }
        else{
            cellOfUp.lbeTimeH.text = String(format: "%02d:", time.1)
            cellOfUp.lbeTimeM.text = String(format: "%02d:",time.2)
            cellOfUp.lbeTimeS.text = String(format: "%02d",time.3)
            
//            time.1 = 0
//            time.2 = 0
//            time.3 = 0
            
            if time.1 == 0 &&  time.2 == 0 && time.3 == 0 {
                cellOfUp.lbeTimeH.text = ""
                cellOfUp.lbeTimeM.text = ""
                cellOfUp.lbeTimeS.text = ""
                cellOfUp.lbeEst.isHidden = true
                cellOfUp.viewJoinCall.isHidden = false
                cellOfUp.imgViewTimeLogo.isHidden = true
                cellOfUp.btnEst.setTitle("Join Call", for: .normal)
                cellOfUp.btnEst.isHidden = false
                cellOfUp.viewJoinCall.backgroundColor = UIColor(red: 56.0 / 255.0, green: 207.0 / 255.0, blue: 124.0 / 255.0, alpha: 1.0)

            }
            else{
                cellOfUp.viewJoinCall.backgroundColor = AppColor.AppGreenishTeal
            }
        }
    
        return cellOfUp
    }
}
    
//MARK: - TableViewDelegate Delegate
extension UpcomingAppointmentsController : UITableViewDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
        return 180
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
               
        tableView.deselectRow(at: indexPath, animated: false)
        let storyboard = UIStoryboard(name: "Appointment", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "JoinCallController") as! JoinCallController
        let data : UpcomingAppointmentModel = self.UpcomingAppointments[indexPath.row]

        vc.PatientName = data.patientName
        vc.PatientProfile = data.patientProfilePic
        let startDate = data.startTime.UTCToLocal(incomingFormat: "dd-MM-yyyy HH:mm:ss", outGoingFormat: "dd-MM-yyyy HH:mm:ss")

        vc.timeDate = startDate
        
        let dateOnly = data.startTime.UTCToLocal(incomingFormat: "dd-MM-yyyy HH:mm:ss", outGoingFormat: "dd-MM-yyyy")

        vc.dateOnly = dateOnly

        
        self.navigationController?.pushViewController(vc, animated: true)
        
        if self.countdownTimer.isValid{
            self.countdownTimer.invalidate()
               }
          
      }

 }*/
